A small module which logs each time a View is used on your site. The intent is to identify Views that are no longer in use and may be safely deleted.

This module adds an INSERT every time a view is used so do not leave it enabled in production for a long time.

Author
----------
drawk - http://www.whatwoulddrupaldo.org/views-audit

Maintainer
------------
Moshe Weitzman -  http://drupal.org/moshe